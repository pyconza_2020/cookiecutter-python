#!/bin/bash
python3 -m venv .venv
poetry install
git init
git remote add origin ssh://git@git.local:9512/{{ cookiecutter.git_group }}/{{ cookiecutter.python_dir_and_filename }}.git
git add docker-build Dockerfile .gitlab-ci.yml .gitignore poetry.lock pyproject.toml README.md {{ cookiecutter.python_dir_and_filename }}/{{ cookiecutter.python_dir_and_filename }}.py .vscode
git commit -m "[ci-skip] Initial commit"
git push --set-upstream origin master