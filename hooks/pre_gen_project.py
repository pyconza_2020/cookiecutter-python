#!/bin/python3
import requests

from subprocess import call

headers = {"PRIVATE-TOKEN": "YxeYbN3mr37GhuSCgYtV"}
GROUP = "{{ cookiecutter.git_group }}"
PROJECT = "{{ cookiecutter.python_dir_and_filename }}"

group_res = requests.get(f"http://git.local/api/v4/groups?search={GROUP}", headers=headers)
project_res = requests.post(f"http://git.local/api/v4/projects", headers=headers, data = {"name": PROJECT, "namespace_id": group_res.json()[0]["id"]})
